# Тестовое приложение на node.js  

запоминает отправленное по ссылке имя
```
curl "http://mgra.fire-mir.ru/remember?answer=Love+you&name=sweetie"
```
и возвращает его по ссылке:
```
curl http://mgra.fire-mir.ru/say
```
## CI-CD в ветке main:

### build_image:
1) Докер логин
2) Сборка образа
3) Пуш в registry с нужным именем
4) замена имени helm чарта на имя проекта
5) упаковка helm чарта в .tgz
6) выгрука нужных переменных для перемещения через артефакты между стейджами

### rules:

для ветки без тега запускать всегда

для stage и prod ставим вместо always  manual чтоб запускать вручную

Далее шаблон для helm 
приложение деплоится в куб с уникальным именем, указываются нужные значения

helm чарт состоит из:
1) database.yaml - быстро развернуть БД mysql в кубе
2) deployment - там initcontainer проверяет доступность бд, и выполнения миграций и приложение не поднимется пока они не пройдут
3) service - доступ к подам по сети
4) ingress -  маршрутизации внешнего трафика к сервисам
5) job - migrate - job для миграций в кубе создается при каждом деплое с первоначальной инициализацией\миграциями

#### stage и prod выглядят аналогично как дев, используется шаблон
